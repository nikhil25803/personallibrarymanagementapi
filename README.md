# PersonalLibraryManagementAPI

A RESTful API developed using Django and Django REST Framework for managing a personal library system. It allows users to add books to their personal library, update book details, delete books, and list the books they have added. Each book entry contains information such as title, authors, publication date, ISBN, and a short description.

## Project Setup

- Clone the repository

```bash
git clone https://gitlab.com/nikhil25803/personallibrarymanagementapi.git
```

- Change the directory

```bash
cd personallibrarymanagementapi
```

- Add a `.env` file. (Can paste the following as well)

```bash
JWT_SECRET_KEY=efb81155b180864799f0cc4031eec04d106456a6
JWT_ALGORITHM=HS256a
```

- Create and activate virtual environment.

```bash
python -m venv env
```

```bash
source env/Scripts/Activate
```

- Install the dependencies

```bash
pip install -r requirements.txt
```

- Run migrations

```bash
python manage.py makemigrations
```

```bash
python manage.py migrate
```

- Start the server

```bash
python manage.py runserver
```

- Open another terminal (to run the test)

```bash
python manage.py test
```

## API Endpoints

- List Books: `GET /api/books/` - Returns a list of all books in the library.

- Add Book: `POST /api/books/` - Adding a new book to the library

- Book Details: `GET /api/books/<isbn>/` - Retrieves the details of a book identified by its ISBN.

- Update Book: `PUT /api/books/<isbn>/` - Updates the details of a specific book.

- Delete Book: `DELETE /api/books/<isbn>/` - Deletes a specific book from the library.

Check Postman Documentation [here](https://documenter.getpostman.com/view/20799971/2sA35EaiNt)

## Features

- Implement ideal file structure for **models**, **views**, **serialzers**, and **test**.
- APIs in `restapi/views.py` to handle the **CRUD** operations.
- Wrote test case for each API endpoint using **Django's testing tools** to ensure they work as expected.
- Implement **token-based authentication** to restrict access to the API, allowing only authenticated users to perform create, update, and delete operations.
