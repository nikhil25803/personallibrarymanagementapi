import uuid
from rest_framework import serializers
from .models import UserModel, BookModel
from .helpers import encrypt_passwords, decrypt_passwords

"""User Regsitration Serializer"""


class UserRegistrationSerializer(serializers.Serializer):
    username = serializers.CharField()
    name = serializers.CharField()
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, data):
        # Validate username
        if data["username"]:
            if UserModel.objects.filter(username=data["username"]).exists():
                raise serializers.ValidationError(detail="Username already exists.")

        # Validate Email
        if data["email"]:
            if UserModel.objects.filter(email=data["email"]).exists():
                raise serializers.ValidationError(
                    detail="A user with same email already exists"
                )

        return data

    def create(self, data):
        new_user = UserModel.objects.create(
            uid="".join(str(uuid.uuid4()).split("-")),
            username=data["username"],
            name=data["name"],
            email=data["email"],
            password=encrypt_passwords(data["password"]),
        )

        new_user.save()
        return new_user


"""User Login Serializer"""


class UserLoginSerialzer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        user_object = UserModel.objects.filter(username=data["username"])
        if not user_object.exists():
            raise serializers.ValidationError(
                detail="User does not exist! Chech credentials again"
            )

        user = user_object.values()[0]
        if not decrypt_passwords(user["password"], data["password"]):
            raise serializers.ValidationError(
                detail="Passwords does not matched. Try again!"
            )
        return user


"""Create Book Serializer"""


class BookPostSerializer(serializers.Serializer):
    title = serializers.CharField()
    authors = serializers.CharField()
    isbn = serializers.CharField()
    description = serializers.CharField()

    def validate(self, data):
        addedby = self.context.get("addedby")
        data["addedby"] = addedby

        if not UserModel.objects.filter(uid=data["addedby"]).exists():
            raise serializers.ValidationError(detail="No user with given uid exists!")

        # Check if ISBN already exists or not
        if BookModel.objects.filter(isbn=data["isbn"]).exists():
            raise serializers.ValidationError(
                detail="A book with the same `isbn` already exists!"
            )

        return data

    def create(self, data):
        new_book = BookModel.objects.create(
            title=data["title"],
            authors=data["authors"],
            isbn=data["isbn"],
            description=data["description"],
            addedby=data["addedby"],
        )

        new_book.save()
        return new_book


"""List Book Serializer"""


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = BookModel
        fields = ["id", "title", "authors", "isbn", "description"]


"""Book Detail Serializer"""


class BookAddedBySerializer(serializers.ModelSerializer):
    class Meta:
        model = UserModel
        fields = ["name", "email"]


class BookDetailSerializer(serializers.ModelSerializer):
    by = serializers.SerializerMethodField()

    class Meta:
        model = BookModel
        exclude = ["addedby"]

    def get_by(self, obj):
        by_user = UserModel.objects.get(uid=obj.addedby)
        by_user_serializer = BookAddedBySerializer(by_user)
        return by_user_serializer.data


"""Book Update Serializer"""


class BookUpdateSerializer(serializers.Serializer):
    title = serializers.CharField()
    authors = serializers.CharField()
    isbn = serializers.CharField()
    description = serializers.CharField()

    def validate(self, data):
        addedby = self.context.get("userid")
        data["userid"] = addedby

        # Check if ISBN already exists or not
        if BookModel.objects.filter(isbn=data["isbn"]).exists():
            raise serializers.ValidationError(
                detail="A book with the same `isbn` already exists!"
            )

        # Check if user authorised to update the details
        if BookModel.objects.filter(isbn=data["isbn"], addedby=data["userid"]).exists():
            raise serializers.ValidationError(
                detail="User is not authorized to update the details"
            )

        return data

    def update(self, instance, validated_data):
        # Update the book instance with validated data
        instance.title = validated_data.get("title", instance.title)
        instance.authors = validated_data.get("authors", instance.authors)
        instance.isbn = validated_data.get("isbn", instance.isbn)
        instance.description = validated_data.get("description", instance.description)
        instance.save()
        return instance
