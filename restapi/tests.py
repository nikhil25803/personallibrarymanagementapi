from django.test import TestCase, Client
from rest_framework import status


class TestEndpoints(TestCase):
    def setUp(self):
        self.client = Client()
        self.token = None
        self.username = "test_username"
        self.name = "Test Name"
        self.email = "test@gmail.com"
        self.password = "testpassword"

    def test_ping(self):
        response = self.client.get("/api/")
        print(response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_register_and_login_user(self):
        # Register a user
        register_data = {
            "username": self.username,
            "name": self.name,
            "email": self.email,
            "password": self.password,
        }
        register_response = self.client.post(
            "http://127.0.0.1:8000/api/user/register",
            register_data,
            content_type="application/json",
        )
        self.assertEqual(register_response.status_code, status.HTTP_201_CREATED)

        # Attempt login with the registered user
        login_data = {
            "username": self.username,
            "password": self.password,
        }
        login_response = self.client.post(
            "http://127.0.0.1:8000/api/user/login",
            login_data,
            content_type="application/json",
        )
        self.assertEqual(login_response.status_code, status.HTTP_200_OK)

        # Store the token to test further ednpoints
        self.token = login_response.json().get("token")

    def test_list_books(self):
        response = self.client.get("http://127.0.0.1:8000/api/books?p=1&page_size=1")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_add_book(self):
        self.test_register_and_login_user()
        headers = {"Authorization": f"{self.token}"}
        data = {
            "title": "Robinson Crusoe",
            "authors": "Daniel Deffoe",
            "isbn": "ABC-123-GHI",
            "description": "Adventerous",
        }
        response = self.client.post(
            "http://127.0.0.1:8000/api/books",
            data,
            content_type="application/json",
            headers=headers,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Test Get Book Details
        isbn = data["isbn"]
        response = self.client.get(f"http://127.0.0.1:8000/api/books/{isbn}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Test Update Book Endpoint
        data["authors"] = "Nikhil Raj"

        response = self.client.put(
            f"http://127.0.0.1:8000/api/books/{isbn}/",
            data,
            content_type="application/json",
            **headers,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Test Delete Book Endpoin
        response = self.client.delete(
            f"http://127.0.0.1:8000/api/books/{isbn}/", **headers
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
