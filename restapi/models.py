from django.db import models
from datetime import date

"""User Model"""


class UserModel(models.Model):
    uid = models.CharField(max_length=100, unique=True, editable=False)
    username = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100, default=None)
    password = models.CharField(max_length=100)

    def __str__(self) -> str:
        return f"{self.name}: {self.username}"


"""Books Model"""


class BookModel(models.Model):
    title = models.CharField(max_length=100, null=False)
    authors = models.CharField(max_length=100, null=False)
    publicationdate = models.DateField(default=date.today)
    isbn = models.CharField(max_length=100, null=False, unique=True)
    description = models.TextField(max_length=1000, default=None)
    addedby = models.CharField(max_length=100, null=False)

    def __str__(self) -> str:
        return f"{self.title}: By {self.authors}"
