from django.contrib import admin
from .models import UserModel, BookModel

# Register your models here.
admin.site.register(UserModel)
admin.site.register(BookModel)
