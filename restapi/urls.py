from django.urls import path
from .views import index, UserAuthentication, ListBookAPIView, BookDetailsAPIView

"""/api URLs here"""

urlpatterns = [
    path("", index, name="ping-test"),
    path("user", UserAuthentication.as_view(), name="user-management"),
    path("user/register", UserAuthentication.as_view(), name="user-register"),
    path("user/login", UserAuthentication.as_view(), name="user-login"),
    path("books", ListBookAPIView.as_view(), name="list-and-create-books"),
    path(
        "books/<str:isbn>/",
        BookDetailsAPIView.as_view(),
        name="book-detail-update-delete",
    ),
]
