from rest_framework import status
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from django.http import JsonResponse
from .helpers import encode_jwt, validate_authentication
from .models import BookModel, UserModel
from .serializers import (
    BookSerializer,
    UserRegistrationSerializer,
    UserLoginSerialzer,
    BookPostSerializer,
    BookDetailSerializer,
    BookUpdateSerializer,
)
from .paginations import CustomPagination


# Ping Test
@api_view(http_method_names=["GET"])
def index(request):
    return JsonResponse(
        data={"details": "Server is up and running"}, status=status.HTTP_200_OK
    )


# User Authentication Endpoints
class UserAuthentication(APIView):
    @staticmethod
    def post(request):
        if request.path.endswith("register"):
            return UserAuthentication.register(request)
        elif request.path.endswith("login"):
            return UserAuthentication.login(request)
        else:
            return JsonResponse(
                data={
                    "status": status.HTTP_400_BAD_REQUEST,
                    "message": "Invalid request.",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

    @staticmethod
    def register(request):
        """
        User Registration POST /api/user/register
        """
        request_data = request.data

        registration_validation = UserRegistrationSerializer(data=request_data)
        if registration_validation.is_valid(raise_exception=True):
            registration_validation.save()
            return JsonResponse(
                data={
                    "message": "User registered successfully. Login to get started!",
                },
                status=status.HTTP_201_CREATED,
            )

        return JsonResponse(
            data={
                "message": "Unable to register user!",
            },
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )

    @staticmethod
    def login(request):
        """
        User Login POST /api/user/login
        """
        request_data = request.data

        login_validation = UserLoginSerialzer(data=request_data)
        if login_validation.is_valid(raise_exception=True):
            user_data = UserModel.objects.filter(
                username=login_validation.data["username"]
            ).values()[0]
            if user_data:
                encoded_data = encode_jwt(payload_data=user_data)
                return JsonResponse(
                    data={
                        "message": "LoggedIn Successfully",
                        "token": encoded_data,
                    },
                    status=status.HTTP_200_OK,
                )
        return JsonResponse(
            data={
                "status": status.HTTP_500_INTERNAL_SERVER_ERROR,
                "message": "Unable to fetch user details.",
            }
        )


# Books Management APIs
class ListBookAPIView(APIView):

    def get(self, request):
        """
        List Books: GET /api/books/
        """

        try:
            paginator = CustomPagination()
            books = BookModel.objects.all().order_by("id")
            results = paginator.paginate_queryset(books, request)
            serialized_books = BookSerializer(results, many=True)
            return paginator.get_paginated_response(serialized_books.data)
        except Exception:
            return JsonResponse(
                data={
                    "message": "Unable to fetch book details",
                },
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

    def post(self, request):
        """
        Add Book: POST /api/books/
        """
        # Getting the incoming data
        incoming_data = request.data

        # Validate JWT Token
        headers = dict(request.headers)
        headers_jwt = headers.get("Authorization")

        if headers_jwt:
            # Validate token and request
            token_data = validate_authentication(headers_jwt)
            book_validation = BookPostSerializer(
                data=incoming_data, context={"addedby": token_data["uid"]}
            )
            if book_validation.is_valid(raise_exception=True):
                book_validation.save()
                return JsonResponse(
                    data={
                        "message": "Book has been added to the library!",
                        "data": book_validation.data,
                    },
                    status=status.HTTP_200_OK,
                )

            return JsonResponse(
                data={
                    "message": "Unable to add book to the library!",
                    "data": None,
                },
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

        return JsonResponse(
            data={
                "message": "No token is provided!",
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


# Book Detail/Update/Delete Endpoints
class BookDetailsAPIView(APIView):
    def get(self, request, isbn):
        """
        Book Details: GET /api/books/<isbn>/
        """
        try:
            if not BookModel.objects.filter(isbn=isbn).exists():
                return JsonResponse(
                    {"message": "Book not found!"},
                    status=status.HTTP_404_NOT_FOUND,
                )
            book = BookModel.objects.get(isbn=isbn)
            serializer = BookDetailSerializer(book)
            return JsonResponse(serializer.data, status=status.HTTP_200_OK)
        except Exception:
            return JsonResponse(
                {"message": "Unable to fetch book details"},
                status=status.HTTP_404_NOT_FOUND,
            )

    def put(self, request, isbn):
        # Getting the incoming data
        incoming_data = request.data

        # Validate JWT Token
        headers = dict(request.headers)
        headers_jwt = headers.get("Authorization")

        if not headers_jwt:
            return JsonResponse(
                data={
                    "status": status.HTTP_400_BAD_REQUEST,
                    "message": "No token is provided!",
                }
            )
        token_data = validate_authentication(headers_jwt)
        try:
            book = BookModel.objects.get(isbn=isbn)
            if not book:
                return JsonResponse(
                    {"detail": "Book not found."}, status=status.HTTP_404_NOT_FOUND
                )
            serializer = BookUpdateSerializer(
                book, data=incoming_data, context={"userid": token_data["uid"]}
            )
            if serializer.is_valid():
                serializer.save()
                return JsonResponse(serializer.data, status=status.HTTP_200_OK)
            return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except BookModel.DoesNotExist:
            return JsonResponse(
                {"detail": "Internal Server Error"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

    def delete(self, request, isbn):
        # Validate JWT Token
        headers = dict(request.headers)
        headers_jwt = headers.get("Authorization")

        if not headers_jwt:
            return JsonResponse(
                data={
                    "status": status.HTTP_400_BAD_REQUEST,
                    "message": "No token is provided!",
                }
            )

        token_data = validate_authentication(headers_jwt)

        try:
            book_data = BookModel.objects.filter(isbn=isbn).values().first()

            # Check if book is available
            if not book_data:
                return JsonResponse(
                    {"detail": "Book not found."}, status=status.HTTP_404_NOT_FOUND
                )

            # Check if user is authorize to delete the book
            if book_data["addedby"] != token_data["uid"]:
                return JsonResponse(
                    {"detail": "User is not authorize to delete the book"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            # Delete book
            book = BookModel.objects.get(isbn=isbn)
            book.delete()

            return JsonResponse(
                {"detail": "Book deleted successfully."},
                status=status.HTTP_200_OK,
            )
        except BookModel.DoesNotExist:
            return JsonResponse(
                {"detail": "Internal server error"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
